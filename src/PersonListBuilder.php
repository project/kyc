<?php

namespace Drupal\kyc;

use Drupal\kyc\Entity\PersonType;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines the list builder for kycs.
 */
class PersonListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['name'] = $this->t('Name');
    $header['type'] = $this->t('Type');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\kyc\Entity\KYCInterface $entity */
    $kyc_person_type = PersonType::load($entity->bundle());

    $row['name']['data'] = Link::fromTextAndUrl($entity->label(), $entity->toUrl());
    $row['type'] = $kyc_person_type->label();

    return $row + parent::buildRow($entity);
  }

}
