<?php

namespace Drupal\kyc\Plugin\QueueWorker;

/**
 * @file
 */

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\kyc\BackendManager;
use Psr\Log\LoggerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Processes tasks for KYC upsert queue worker.
 *
 * @QueueWorker(
 *   id = "upsert_kyc_queue",
 *   title = @Translation("KYC upsert queue worker"),
 *   cron = {"time" = 60}
 * )
 */
class UpsertKYCQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface;
   */
  protected $entityTypeManager;


  /**
   * KYC backend manager.
   *
   * @var \Drupal\kyc\BackendManager
   */
  protected $kycBackendManager;

  /**
   * Logger factory.
   *
   * @var Psr\Log\LoggerInterface
   */
  private LoggerInterface $logger;

  /**
   * Creates a new NodePublishBase object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    BackendManager $kyc_backend_manager,
    LoggerChannelFactory $logger_factory,
    array $configuration,
    $plugin_id,
    $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger_factory->get('kyc');
    $this->kycBackendManager = $kyc_backend_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {

    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.kyc_backend'),
      $container->get('logger.factory'),
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($item) {
    /** @var \Drupal\kyc\BackendInterface @backend */
    $backend = $this->kycBackendManager->createInstance($item['backend']);
    if (empty($backend)) {
      throw new \Exception('Can\'t find backend with name ' . $item['backend']);
    }

    $storage = $this->entityTypeManager->getStorage($item['type']);
    if (empty($storage)) {
      throw new \Exception('Can\'t find storage for entity type ' . $item['type']);
    }

    $entity = $storage->load($item['id']);
    if (empty($entity)) {
      throw new \Exception('Can\'t find entity with ID ' . $item['id']);
    }

    try {
      $backend->upsert($entity);
    }
    catch (\Exception $e) {
      $this->logger->error('Error upserting KYC entity with ID ' . $item['id'] . ': ' . $e->getMessage());
    }
  }

}
