<?php

namespace Drupal\kyc\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\kyc\Entity\KYCInterface;
use Drupal\Core\Entity\Controller\EntityViewController;
use Drupal\Core\Entity\EntityStorageInterface;


/**
 * Returns responses for KYC routes.
 */
class KYCController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * Constructs a KYCController object.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   */
  public function __construct(DateFormatterInterface $date_formatter, RendererInterface $renderer, EntityRepositoryInterface $entity_repository) {
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
    $this->entityRepository = $entity_repository;
  }

  /**
   * Displays a KYC revision.
   *
   * @param \Drupal\kyc\Entity\KYCInterface $kyc_revision
   *   The kyc revision.
   *
   * @return array
   *   An array suitable for \Drupal\Core\Render\RendererInterface::render().
   */
  public function revisionShow(KYCInterface $kyc_revision) {
    $kyc_view_controller = new EntityViewController($this->entityTypeManager(), $this->renderer, $this->currentUser(), $this->entityRepository);
    $page = $kyc_view_controller->view($kyc_revision);
    unset($page['kycs'][$kyc_revision->id()]['#cache']);
    return $page;
  }

  /**
   * Page title callback for a KYC revision.
   *
   * @param \Drupal\kyc\Entity\KYCInterface $kyc_revision
   *   The KYC revision.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle(KYCInterface $kyc_revision) {
    return $this->t('Revision of %title from %date', [
      '%title' => $kyc_revision->label(),
      '%date' => $this->dateFormatter->format($kyc_revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * Renders revision overview for persons
   *
   * @param KYCInterface $kyc_person
   * @return void
   */
  public function personRevisionOverview(KYCInterface $kyc_person) {
    return $this->revisionOverview($kyc_person);
  }

  /**
   * Renders revision overview for documents
   *
   * @param KYCInterface $kyc_document
   * @return void
   */
  public function documentRevisionOverview(KYCInterface $kyc_document) {
    return $this->revisionOverview($kyc_document);
  }

  /**
   * Renders revision overview for businesses
   *
   * @param KYCInterface $kyc_business
   * @return void
   */
  public function businessRevisionOverview(KYCInterface $kyc_business) {
    return $this->revisionOverview($kyc_business);
  }

  /**
   * Generates an overview table of older revisions of a KYC.
   *
   * @param \Drupal\kyc\Entity\KYCInterface $kyc
   *   A KYC object.
   *
   * @return array
   *   An array as expected by \Drupal\Core\Render\RendererInterface::render().
   */
  public function revisionOverview(KYCInterface $kyc) {
    $kyc = $this->entityRepository->getActive($kyc->getEntityTypeId(), $kyc->id());
    $entity_type = $kyc->getEntityTypeId();
    $storage = $this->entityTypeManager()->getStorage($entity_type);

    $build['#title'] = $this->t('Revisions for %title', ['%title' => $kyc->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $rows = [];
    $current_revision_displayed = FALSE;

    foreach ($this->getRevisionIds($kyc, $storage) as $revision_id) {
      /** @var \Drupal\kyc\Entity\KYCInterface $revision */
      $revision = $storage->loadRevision($revision_id);
      $username = [
        '#theme' => 'username',
        '#account' => $revision->getRevisionUser(),
      ];

      // Use revision link to link to revisions that are not active.
      $date = $this->dateFormatter->format($revision->revision_timestamp->value, 'short');

      // We treat also the latest translation-affecting revision as current
      // revision, if it was the default revision, as its values for the
      // current language will be the same of the current default revision in
      // this case.
      $is_current_revision = $revision->isDefaultRevision() || (!$current_revision_displayed && $revision->wasDefaultRevision());
      if (!$is_current_revision) {
        $link = Link::fromTextAndUrl($date, new Url('entity.' . $entity_type . '.revision', [
          $entity_type => $kyc->id(),
          'kyc_revision' => $revision_id,
        ]))->toString();
      }
      else {
        $link = $kyc->toLink($date)->toString();
        $current_revision_displayed = TRUE;
      }

      $row = [];
      $column = [
        'data' => [
          '#type' => 'inline_template',
          '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
          '#context' => [
            'date' => $link,
            'username' => $this->renderer->renderPlain($username),
            'message' => ['#markup' => $revision->revision_log->value, '#allowed_tags' => Xss::getHtmlTagList()],
          ],
        ],
      ];
      // @todo Simplify once https://www.drupal.org/kyc/2334319 lands.
      $this->renderer->addCacheableDependency($column['data'], $username);
      $row[] = $column;

      if ($is_current_revision) {
        $row[] = [
          'data' => [
            '#prefix' => '<em>',
            '#markup' => $this->t('Current revision'),
            '#suffix' => '</em>',
          ],
        ];

        $rows[] = [
          'data' => $row,
          'class' => ['revision-current'],
        ];
      }
      else {
        $links = [];
        if ($revision->access('revert revision')) {
          $links['revert'] = [
            'title' => $revision_id < $kyc->getRevisionId() ? $this->t('Revert') : $this->t('Set as current revision'),
            'url' => Url::fromRoute($entity_type . '.revision_revert_confirm', [
              $entity_type => $kyc->id(),
              'kyc_revision' => $revision_id,
            ]),
          ];
        }

        $row[] = [
          'data' => [
            '#type' => 'operations',
            '#links' => $links,
          ],
        ];

        $rows[] = $row;
      }
    }

    $build['kyc_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
      '#attributes' => ['class' => ['kyc-revision-table']],
    ];

    $build['pager'] = ['#type' => 'pager'];

    return $build;
  }

  /**
   * Gets a list of kyc revision IDs for a specific kyc.
   *
   * @param \Drupal\kyc\Entity\KYCInterface $kyc
   *   The kyc entity.
   * @param \Drupal\kyc\EntityStorageInterface $storage
   *   The kyc storage handler.
   *
   * @return int[]
   *   KYC revision IDs (in descending order).
   */
  protected function getRevisionIds(KYCInterface $kyc, EntityStorageInterface $storage) {
    $result = $storage->getQuery()
      ->accessCheck(TRUE)
      ->allRevisions()
      ->condition($kyc->getEntityType()->getKey('id'), $kyc->id())
      ->sort($kyc->getEntityType()->getKey('revision'), 'DESC')
      ->pager(50)
      ->execute();
    return array_keys($result);
  }

}
