<?php

namespace Drupal\kyc;

use Drupal\Component\Plugin\PluginBase;

/**
 * Provides a base implementation for a Backend plugin.
 *
 * @see plugin_api
 *
 * @ingroup queue
 */
abstract class BackendBase extends PluginBase implements BackendInterface {

}
