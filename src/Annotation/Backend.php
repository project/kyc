<?php

namespace Drupal\kyc\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Backend plugin annotation object.
 *
 * @Annotation
 */
class Backend extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the backend type.
   *
   * @var \Drupal\Core\Annotation\Translation
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A short description of the backend type.
   *
   * @var \Drupal\Core\Annotation\Translation
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The name of the backend class.
   *
   * This is not provided manually, it will be added by the discovery mechanism.
   *
   * @var string
   */
  public $class;

  /**
   * An integer to determine the weight of this backend.
   *
   * Optional. This is relative to other backends and their execution.
   *
   * @var int
   */
  public $weight = NULL;

}
