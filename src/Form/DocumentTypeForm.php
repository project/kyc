<?php

namespace Drupal\kyc\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity\Form\EntityDuplicateFormTrait;
use Drupal\state_machine\WorkflowManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DocumentTypeForm extends BundleEntityFormBase {

  use EntityDuplicateFormTrait;

  /**
   * The workflow manager.
   *
   * @var \Drupal\state_machine\WorkflowManagerInterface
   */
  protected $workflowManager;

  /**
   * Constructs a new DocumenTypeForm object.
   *
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   */
  public function __construct(WorkflowManagerInterface $workflow_manager) {
    $this->workflowManager = $workflow_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.workflow')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\kyc\Entity\PersonTypeInterface $kyc_document_type */
    $kyc_document_type = $this->entity;

    $workflows = $this->workflowManager->getGroupedLabels('kyc_document');

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $kyc_document_type->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $kyc_document_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\kyc\Entity\PersonType::load',
      ],
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#disabled' => !$kyc_document_type->isNew(),

    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t('This text will be displayed on the <em>Add Person</em> page.'),
      '#default_value' => $kyc_document_type->getDescription(),
    ];
    $form['workflow'] = [
      '#type' => 'select',
      '#title' => $this->t('Workflow'),
      '#options' => $workflows,
      '#default_value' => $kyc_document_type->getWorkflowId(),
      '#description' => $this->t('Used by all documents of this type.'),
    ];
    $form['revisions'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create new revisions'),
      '#default_value' => $kyc_document_type->shouldCreateNewRevision(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $this->entity->setNewRevision($form_state->getValue('revisions'));
    $this->entity->save();
    $this->postSave($this->entity, $this->operation);

    $this->messenger()->addMessage($this->t('Saved the %label document type.', [
      '%label' => $this->entity->label(),
    ]));
    $form_state->setRedirect('entity.kyc_document_type.collection');
  }

}
