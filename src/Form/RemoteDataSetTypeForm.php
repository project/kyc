<?php

namespace Drupal\kyc\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity\Form\EntityDuplicateFormTrait;

/**
 * Remote Data Set type add and edit forms.
 */
class RemoteDataSetTypeForm extends BundleEntityFormBase {

  use EntityDuplicateFormTrait;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\kyc\Entity\RemoteDataSetTypeInterface $kyc_remote_data_set_type */
    $kyc_remote_data_set_type = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $kyc_remote_data_set_type->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $kyc_remote_data_set_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\kyc\Entity\RemoteDataSetType::load',
      ],
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#disabled' => !$kyc_remote_data_set_type->isNew(),

    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t('This text will be displayed on the <em>Add KYC</em> page.'),
      '#default_value' => $kyc_remote_data_set_type->getDescription(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $this->entity->save();
    $this->postSave($this->entity, $this->operation);

    $this->messenger()->addMessage($this->t('Saved the %label Remote Data Set type.', [
      '%label' => $this->entity->label(),
    ]));
    $form_state->setRedirect('entity.kyc_remote_data_set_type.collection');
  }

}
