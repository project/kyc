<?php

namespace Drupal\kyc\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity\Form\EntityDuplicateFormTrait;
use Drupal\state_machine\WorkflowManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PersonTypeForm extends BundleEntityFormBase {

  use EntityDuplicateFormTrait;

  /**
   * The workflow manager.
   *
   * @var \Drupal\state_machine\WorkflowManagerInterface
   */
  protected $workflowManager;

  /**
   * Constructs a new PersonTypeForm object.
   *
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   */
  public function __construct(WorkflowManagerInterface $workflow_manager) {
    $this->workflowManager = $workflow_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.workflow')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\kyc\Entity\PersonTypeInterface $kyc_person_type */
    $kyc_person_type = $this->entity;

    $workflows = $this->workflowManager->getGroupedLabels('kyc_person');

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $kyc_person_type->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $kyc_person_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\kyc\Entity\PersonType::load',
      ],
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#disabled' => !$kyc_person_type->isNew(),

    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t('This text will be displayed on the <em>Add Person</em> page.'),
      '#default_value' => $kyc_person_type->getDescription(),
    ];

    $form['workflow'] = [
      '#type' => 'select',
      '#title' => $this->t('Workflow'),
      '#options' => $workflows,
      '#default_value' => $kyc_person_type->getWorkflowId(),
      '#description' => $this->t('Used by all persons of this type.'),
    ];
    $form['revisions'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create new revisions'),
      '#default_value' => $kyc_person_type->shouldCreateNewRevision(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $this->entity->setNewRevision($form_state->getValue('revisions'));
    $this->entity->save();

    $this->postSave($this->entity, $this->operation);

    $this->messenger()->addMessage($this->t('Saved the %label person type.', [
      '%label' => $this->entity->label(),
    ]));
    $form_state->setRedirect('entity.kyc_person_type.collection');
  }

}
