<?php

namespace Drupal\kyc\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Defines the interface for Remote Data Set's.
 */
interface RemoteDataSetInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the remote ID.
   *
   * @return string
   *   The remote ID.
   */
  public function getRemoteId();

  /**
   * Sets the remote ID.
   *
   * @param string $remote_id
   *   The remote ID.
   *
   * @return $this
   */
  public function setRemoteId($remote_id);

  /**
   * Gets the remote state.
   *
   * @return string
   *   The remote state.
   */
  public function getRemoteState();

  /**
   * Sets the remote state.
   *
   * @param string $remote_state
   *   The remote state.
   *
   * @return $this
   */
  public function setRemoteState($remote_state);

  /**
   * Gets the Business's creation timestamp.
   *
   * @return int
   *   The Business's creation timestamp.
   */
  public function getCreatedTime();

  /**
   * Sets the Business's creation timestamp.
   *
   * @param int $timestamp
   *   The Business's creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime($timestamp);

}
