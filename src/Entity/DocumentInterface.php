<?php

namespace Drupal\kyc\Entity;

/**
 * Defines the interface for Document'ss.
 */
interface DocumentInterface extends KYCInterface {

  /**
   * Gets the Document's first name.
   *
   * @return string
   *   The Document's name.
   */
  public function getName();

  /**
   * Sets the Document's first name.
   *
   * @param string $name
   *   The Document's name.
   *
   * @return $this
   */
  public function setName($name);

}
