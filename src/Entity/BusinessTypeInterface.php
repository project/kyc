<?php

namespace Drupal\kyc\Entity;

/**
 * Defines the interface for kyc types.
 */
interface BusinessTypeInterface extends KYCTypeInterface {

}
