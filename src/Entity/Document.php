<?php

namespace Drupal\kyc\Entity;

use Drupal\user\EntityOwnerTrait;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the document entity class.
 *
 * @ContentEntityType(
 *   id = "kyc_document",
 *   label = @Translation("Document"),
 *   label_collection = @Translation("Documents"),
 *   label_singular = @Translation("document"),
 *   label_plural = @Translation("documents"),
 *   label_count = @PluralTranslation(
 *     singular = "@count document",
 *     plural = "@count documents",
 *   ),
 *   bundle_label = @Translation("Document type"),
 *   handlers = {
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "query_access" = "Drupal\entity\QueryAccess\QueryAccessHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\kyc\DocumentListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\kyc\Form\DocumentForm",
 *       "add" = "Drupal\kyc\Form\DocumentForm",
 *       "edit" = "Drupal\kyc\Form\DocumentForm",
 *       "duplicate" = "Drupal\kyc\Form\DocumentForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\AdminHtmlRouteProvider",
 *       "delete-multiple" = "Drupal\entity\Routing\DeleteMultipleRouteProvider",
 *     }
 *   },
 *   base_table = "kyc_document",
 *   revision_table = "kyc_document_revision",
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer document",
 *   permission_granularity = "bundle",
 *   translatable = FALSE,
 *   entity_keys = {
 *     "id" = "document_id",
 *     "revision" = "revision_id",
 *     "uuid" = "uuid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "langcode" = "langcode",
 *     "owner" = "uid",
 *     "uid" = "uid",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "canonical" = "/admin/people/kyc/document/{kyc_document}",
 *     "add-page" = "/admin/people/kyc/document/add",
 *     "add-form" = "/admin/people/kyc/document/add/{kyc_document_type}",
 *     "edit-form" = "/admin/people/kyc/document/{kyc_document}/edit",
 *     "duplicate-form" = "/admin/people/kyc/document/{kyc_document}/duplicate",
 *     "delete-form" = "/admin/people/kyc/document/{kyc_document}/delete",
 *     "collection" = "/admin/people/kyc/document",
 *     "version-history" = "/admin/people/kyc/document/{kyc_document}/revisions",
 *     "revision" = "/admin/people/kyc/document/{kyc_document}/revisions/{kyc_document_revision}/view",
 *   },
 *   bundle_entity_type = "kyc_document_type",
 *   field_ui_base_route = "entity.kyc_document_type.edit_form",
 * )
 */
class Document extends RevisionableContentEntityBase implements DocumentInterface {

  use EntityOwnerTrait;
  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getState() {
    return $this->get('state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setState($state) {
    $this->set('state', $state);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($remote_id) {
    $this->set('name', $remote_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteDataSetsOfType($bundle) {
    $remote_data_sets = [];
    foreach ($this->getRemoteDataSets() as $remote_data_set) {
      if ($remote_data_set->bundle() === $bundle) {
        $remote_data_sets[] = $remote_data_set;
      }
    }
    return $remote_data_sets;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteDataSets() {
    return $this->get('remote_data_sets')->referencedEntities();
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteDataSet($bundle) {
    $remote_data_sets = $this->getRemoteDataSetsOfType($bundle);
    return reset($remote_data_sets);
  }

  /**
   * {@inheritdoc}
   */
  public function setRemoteDataSets($remote_data_sets) {
    $this->set('remote_data_sets', $remote_data_sets);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addRemoteDataSet($remote_data_set) {
    $remote_data_sets = $this->getRemoteDataSets();
    $remote_data_sets[] = $remote_data_set;
    $this->setRemoteDataSets($remote_data_sets);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Type'))
      ->setDescription(t('Document\'s type.'))
      ->setSetting('target_type', 'kyc_document_type')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 0,
      ])
      ->setReadOnly(TRUE);

    $fields['uid']
      ->setLabel(t('Owner'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    // The business backreference, populated by Business::postSave().
    $fields['business_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Business'))
      ->setRevisionable(TRUE)
      ->setDescription(t('The parent business.'))
      ->setSetting('target_type', 'kyc_business')
      ->setReadOnly(TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // The person backreference, populated by Person::postSave().
    $fields['person_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Person'))
      ->setRevisionable(TRUE)
      ->setDescription(t('The parent person.'))
      ->setSetting('target_type', 'kyc_person')
      ->setReadOnly(TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['state'] = BaseFieldDefinition::create('state')
      ->setLabel(t('State'))
      ->setDescription(t('Verification state.'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -10,
      ])
      ->setDisplayOptions('view', [
        'type' => 'state_transition_form',
        'settings' => [
          'require_confirmation' => TRUE,
          'use_modal' => TRUE,
        ],
        'weight' => -10,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setSetting('workflow_callback', ['\Drupal\kyc\Entity\Document', 'getWorkflowId']);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('Document\'s name.'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['file'] = BaseFieldDefinition::create('file')
      ->setLabel(t('Files'))
      ->setDescription(t('The document files.'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setSetting('file_extensions', 'pdf jpg jpeg png gif')
      ->setSetting('uri_scheme', 'private')
      ->setDisplayOptions('form', [
        'type' => 'file_generic',
        'weight' => 0,
      ])
      ->setDisplayOptions('view', [
        'type' => 'file_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['remote_data_sets'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Remote Data Sets'))
      ->setDescription(t('The remote data sets associated with the document.'))
      ->setSetting('target_type', 'kyc_remote_data_set')
      ->setSetting('handler', 'default')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['path'] = BaseFieldDefinition::create('path')
      ->setLabel(t('URL alias'))
      ->setDescription(t('The document URL alias.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setCustomStorage(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when the Document was created.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time when the Document was last edited.'));

    return $fields;
  }

  /**
   * Gets the workflow ID for the state field.
   *
   * @param \Drupal\kyc\Entity\DocumentInterface $document
   *   The document.
   *
   * @return string
   *   The workflow ID.
   */
  public static function getWorkflowId(DocumentInterface $document) {
    $document_type = DocumentType::load($document->bundle());
    return $document_type?->getWorkflowId() ?? 'kyc_document_default';
  }

}
