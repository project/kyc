<?php

namespace Drupal\kyc\Entity;

use CommerceGuys\Addressing\AddressFormat\AddressField;
use CommerceGuys\Addressing\AddressFormat\FieldOverride;
use Drupal\user\EntityOwnerTrait;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\address\AddressInterface;

/**
 * Defines the kyc_business entity class.
 *
 * @ContentEntityType(
 *   id = "kyc_business",
 *   label = @Translation("Business"),
 *   label_collection = @Translation("Businesses"),
 *   label_singular = @Translation("business"),
 *   label_plural = @Translation("businesses"),
 *   label_count = @PluralTranslation(
 *     singular = "@count business",
 *     plural = "@count businesses",
 *   ),
 *   bundle_label = @Translation("Business type"),
 *   handlers = {
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "query_access" = "Drupal\entity\QueryAccess\QueryAccessHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\kyc\BusinessListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\kyc\Form\BusinessForm",
 *       "add" = "Drupal\kyc\Form\BusinessForm",
 *       "edit" = "Drupal\kyc\Form\BusinessForm",
 *       "duplicate" = "Drupal\kyc\Form\BusinessForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\AdminHtmlRouteProvider",
 *       "delete-multiple" = "Drupal\entity\Routing\DeleteMultipleRouteProvider",
 *     }
 *   },
 *   base_table = "kyc_business",
 *   revision_table = "kyc_business_revision",
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer kyc_business",
 *   permission_granularity = "bundle",
 *   translatable = FALSE,
 *   entity_keys = {
 *     "id" = "business_id",
 *     "revision" = "revision_id",
 *     "uuid" = "uuid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "langcode" = "langcode",
 *     "owner" = "uid",
 *     "uid" = "uid",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "canonical" = "/admin/people/kyc/business/{kyc_business}",
 *     "add-page" = "/admin/people/kyc/business/add",
 *     "add-form" = "/admin/people/kyc/business/add/{kyc_business_type}",
 *     "edit-form" = "/admin/people/kyc/business/{kyc_business}/edit",
 *     "duplicate-form" = "/admin/people/kyc/business/{kyc_business}/duplicate",
 *     "delete-form" = "/admin/people/kyc/business/{kyc_business}/delete",
 *     "collection" = "/admin/people/kyc/business",
 *     "version-history" = "/admin/people/kyc/business/{kyc_business}/revisions",
 *     "revision" = "/admin/people/kyc/business/{kyc_business}/revisions/{kyc_business_revision}/view",
 *   },
 *   bundle_entity_type = "kyc_business_type",
 *   field_ui_base_route = "entity.kyc_business_type.edit_form",
 * )
 */
class Business extends RevisionableContentEntityBase implements BusinessInterface {

  use EntityOwnerTrait;
  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getState() {
    return $this->get('state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setState($state) {
    $this->set('state', $state);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmail() {
    $email = $this->get('mail')->value;
    // Defaults to the site email if the kyc email isn't set.
    if (empty($email)) {
      $email = \Drupal::config('system.site')->get('mail') ?: ini_get('sendmail_from');
    }

    return $email;
  }

  /**
   * {@inheritdoc}
   */
  public function setEmail($mail) {
    $this->set('mail', $mail);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAddress() {
    return $this->get('address')->first();
  }

  /**
   * {@inheritdoc}
   */
  public function setAddress(AddressInterface $address) {
    // $this->set('address', $address) results in the address being appended
    // to the item list, instead of replacing the existing first item.
    $this->address = $address;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRegistrationNumber() {
    return $this->get('registration_number')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRegistrationNumber($registration_number) {
    $this->set('registration_number', $registration_number);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDocuments() {
    return $this->get('documents')->referencedEntities();
  }

  /**
   * {@inheritdoc}
   */
  public function setDocuments($documents) {
    $this->set('documents', $documents);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPersons() {
    return $this->get('persons')->referencedEntities();
  }

  /**
   * {@inheritdoc}
   */
  public function setPersons($persons) {
    $this->set('persons', $persons);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPersonsOfType($bundle) {
    $persons = [];
    foreach ($this->getPersons() as $person) {
      if ($person->bundle() === $bundle) {
        $persons[] = $person;
      }
    }
    return $persons;
  }

  /**
   * {@inheritdoc}
   */
  public function getPerson($bundle) {
    $persons = $this->getPersonsOfType($bundle);
    return reset($persons);
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteDataSetsOfType($bundle) {
    $remote_data_sets = [];
    foreach ($this->getRemoteDataSets() as $remote_data_set) {
      if ($remote_data_set->bundle() === $bundle) {
        $remote_data_sets[] = $remote_data_set;
      }
    }
    return $remote_data_sets;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteDataSets() {
    return $this->get('remote_data_sets')->referencedEntities();
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteDataSet($bundle) {
    $remote_data_sets = $this->getRemoteDataSetsOfType($bundle);
    return reset($remote_data_sets);
  }

  /**
   * {@inheritdoc}
   */
  public function setRemoteDataSets($remote_data_sets) {
    $this->set('remote_data_sets', $remote_data_sets);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addRemoteDataSet($remote_data_set) {
    $remote_data_sets = $this->getRemoteDataSets();
    $remote_data_sets[] = $remote_data_set;
    $this->setRemoteDataSets($remote_data_sets);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    // Ensure there's a back-reference on each person.
    foreach ($this->persons as $person) {
      $entity = $person->entity;
      if ($entity && $entity->business_id->isEmpty()) {
        $entity->business_id = $this->id();
        $entity->save();
      }
    }

    // Ensure there's a back-reference on each document.
    foreach ($this->documents as $document) {
      $entity = $document->entity;
      if ($entity && $entity->business_id->isEmpty()) {
        $entity->business_id = $this->id();
        $entity->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Type'))
      ->setDescription(t('The business type.'))
      ->setSetting('target_type', 'kyc_business_type')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 0,
      ])
      ->setReadOnly(TRUE);

    $fields['uid']
      ->setLabel(t('Owner'))
      ->setDescription(t('The business owner.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['state'] = BaseFieldDefinition::create('state')
      ->setLabel(t('State'))
      ->setDescription(t('Verification state.'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -10,
      ])
      ->setDisplayOptions('view', [
        'type' => 'state_transition_form',
        'settings' => [
          'require_confirmation' => TRUE,
          'use_modal' => TRUE,
        ],
        'weight' => -10,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setSetting('workflow_callback', ['\Drupal\kyc\Entity\Business', 'getWorkflowId']);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('Business\' registered name.'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['mail'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Email'))
      ->setDescription(t('Business\' email.'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'email_default',
        'weight' => 1,
      ])
      ->setDisplayOptions('view', [
        'type' => 'email_mailto',
        'weight' => 1,
      ])
      ->setSetting('display_description', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['telephone'] = BaseFieldDefinition::create('telephone')
      ->setLabel(t('Telephone number'))
      ->setDescription(t('Business\' telephone number.'))
      ->setRequired(FALSE)
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'telephone_default',
        'weight' => 2,
      ])
      ->setDisplayOptions('view', [
        'type' => 'telephone_link',
        'weight' => 2,
      ])
      ->setSetting('display_description', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['address'] = BaseFieldDefinition::create('address')
      ->setLabel(t('Address'))
      ->setDescription(t('Business\' address.'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setSetting('field_overrides', [
        AddressField::GIVEN_NAME => ['override' => FieldOverride::HIDDEN],
        AddressField::ADDITIONAL_NAME => ['override' => FieldOverride::HIDDEN],
        AddressField::FAMILY_NAME => ['override' => FieldOverride::HIDDEN],
        AddressField::ORGANIZATION => ['override' => FieldOverride::HIDDEN],
        AddressField::ADDRESS_LINE3 => ['override' => FieldOverride::HIDDEN],
      ])
      ->setDisplayOptions('form', [
        'type' => 'address_default',
        'weight' => 3,
      ])
      ->setDisplayOptions('view', [
        'type' => 'address_default',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['registration_number'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Registration number'))
      ->setDescription(t('Business\' registration number. For companies this is a company number as assigned by the government registrar or tax authority.'))
      ->setRequired(FALSE)
      ->setRevisionable(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 4,
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['registration_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Registration date'))
      ->setDescription(t('The date the business was registered with government registrar.'))
      ->setRequired(FALSE)
      ->setRevisionable(TRUE)
      ->setSettings([
        'datetime_type' => 'date'
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => 5,
      ])
      ->setDisplayOptions('view', [
        'type' => 'datetime_default',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['registration_country'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Registration country'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setSetting('allowed_values_function', ['\Drupal\kyc\Entity\Business', 'getAvailableCountries'])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 6,
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['trading_countries'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Trading countries'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setRevisionable(TRUE)
      ->setSetting('allowed_values_function', ['\Drupal\kyc\Entity\Business', 'getAvailableCountries'])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 7,
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['persons'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Persons'))
      ->setRevisionable(TRUE)
      ->setDescription(t('The persons associated with the business.'))
      ->setSetting('target_type', 'kyc_person')
      ->setSetting('handler', 'default')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
        'settings' => [
          'view_mode' => 'default',
        ],
        'weight' => 8,
      ])
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_complex',
        'settings' => [
          'override_labels' => TRUE,
          'label_singular' => 'person',
          'label_plural' => 'persons',
          'add_mode' => 'simple',
          'form_mode' => 'default',
          'allow_new' => TRUE,
          'allow_existing' => TRUE,
          'allow_duplicate' => TRUE,
          'allow_reference' => TRUE,
          'allow_reference_override' => TRUE,
          'available_types' => ['kyc_person'],
          'available_fields' => [
            'name' => 'name',
            'mail' => 'mail',
            'address' => 'address',
          ],
        ],
        'weight' => 8,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['documents'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Documents'))
      ->setRevisionable(TRUE)
      ->setDescription(t('The documents associated with the business.'))
      ->setSetting('target_type', 'kyc_document')
      ->setSetting('handler', 'default')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
        'settings' => [
          'view_mode' => 'default',
        ],
        'weight' => 8,
      ])
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_complex',
        'settings' => [
          'override_labels' => TRUE,
          'label_singular' => 'document',
          'label_plural' => 'documents',
          'add_mode' => 'simple',
          'form_mode' => 'default',
          'allow_new' => TRUE,
          'allow_existing' => TRUE,
          'allow_duplicate' => TRUE,
          'allow_reference' => TRUE,
          'allow_reference_override' => TRUE,
          'available_types' => ['kyc_document'],
          'available_fields' => [
            'name' => 'name',
          ],
        ],
        'weight' => 8,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['remote_data_sets'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Remote Data Sets'))
      ->setDescription(t('The remote data sets associated with the business.'))
      ->setSetting('target_type', 'kyc_remote_data_set')
      ->setSetting('handler', 'default')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['path'] = BaseFieldDefinition::create('path')
      ->setLabel(t('URL alias'))
      ->setDescription(t('The business URL alias.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setCustomStorage(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when the vusiness was created.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time when the vusiness was last edited.'));

    return $fields;
  }

  /**
   * Gets the allowed values for the 'billing_countries' base field.
   *
   * @return array
   *   The allowed values.
   */
  public static function getAvailableCountries() {
    return \Drupal::service('address.country_repository')->getList();
  }

  /**
   * Gets the workflow ID for the state field.
   *
   * @param \Drupal\kyc\Entity\BusinessInterface $business
   *   The person.
   *
   * @return string
   *   The workflow ID.
   */
  public static function getWorkflowId(BusinessInterface $business) {
    $business_type = PersonType::load($business->bundle());
    return $business_type?->getWorkflowId() ?? 'kyc_business_default';
  }

}
