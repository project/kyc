<?php

namespace Drupal\kyc\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the kyc type entity class.
 *
 * @ConfigEntityType(
 *   id = "kyc_person_type",
 *   label = @Translation("Person type"),
 *   label_collection = @Translation("Person types"),
 *   label_singular = @Translation("person type"),
 *   label_plural = @Translation("person types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count person type",
 *     plural = "@count person types",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\entity\BundleEntityAccessControlHandler",
 *     "list_builder" = "Drupal\kyc\PersonTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\kyc\Form\PersonTypeForm",
 *       "edit" = "Drupal\kyc\Form\PersonTypeForm",
 *       "duplicate" = "Drupal\kyc\Form\PersonTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer kyc_person_type",
 *   config_prefix = "kyc_person_type",
 *   bundle_of = "kyc_person",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "description",
 *     "new_revision",
 *     "workflow",
 *   },
 *   links = {
 *     "add-form" = "/admin/config/people/kyc/person-type/add",
 *     "edit-form" = "/admin/config/people/kyc/person-type/{kyc_person_type}/edit",
 *     "duplicate-form" = "/admin/config/people/kyc/person-type/{kyc_person_type}/duplicate",
 *     "delete-form" = "/admin/config/people/kyc/person-type/{kyc_person_type}/delete",
 *     "collection" = "/admin/config/people/kyc/person-type",
 *   }
 * )
 */
class PersonType extends ConfigEntityBundleBase implements PersonTypeInterface {

  /**
   * The person type workflow ID.
   *
   * @var string
   */
  protected $workflow;

  /**
   * A brief description of this person type.
   *
   * @var string
   */
  protected $description;

  /**
   * Default value of the 'Create new revision' checkbox of this node type.
   *
   * @var bool
   */
  protected $new_revision = TRUE;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWorkflowId() {
    return $this->workflow;
  }

  /**
   * {@inheritdoc}
   */
  public function setWorkflowId($workflow_id) {
    $this->workflow = $workflow_id;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setNewRevision($new_revision) {
    $this->new_revision = $new_revision;
  }

  /**
   * {@inheritdoc}
   */
  public function shouldCreateNewRevision() {
    return $this->new_revision;
  }

}
