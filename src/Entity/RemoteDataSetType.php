<?php

namespace Drupal\kyc\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the kyc type entity class.
 *
 * @ConfigEntityType(
 *   id = "kyc_remote_data_set_type",
 *   label = @Translation("Remote Data Set type"),
 *   label_collection = @Translation("Remote Data Set types"),
 *   label_singular = @Translation("remote data set type"),
 *   label_plural = @Translation("remote data set types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count remote data set type",
 *     plural = "@count remote data set types",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\entity\BundleEntityAccessControlHandler",
 *     "list_builder" = "Drupal\kyc\RemoteDataSetTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\kyc\Form\RemoteDataSetTypeForm",
 *       "edit" = "Drupal\kyc\Form\RemoteDataSetTypeForm",
 *       "duplicate" = "Drupal\kyc\Form\RemoteDataSetTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer kyc_remote_data_set_type",
 *   config_prefix = "kyc_remote_data_set_type",
 *   bundle_of = "kyc_remote_data_set",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "description",
 *   },
 *   links = {
 *     "add-form" = "/admin/config/people/kyc/remote-data-set-type/add",
 *     "edit-form" = "/admin/config/people/kyc/remote-data-set-type/{kyc_remote_data_set_type}/edit",
 *     "duplicate-form" = "/admin/config/people/kyc/remote-data-set-type/{kyc_remote_data_set_type}/duplicate",
 *     "delete-form" = "/admin/config/people/kyc/remote-data-set-type/{kyc_remote_data_set_type}/delete",
 *     "collection" = "/admin/config/people/kyc/remote-data-set-type",
 *   }
 * )
 */
class RemoteDataSetType extends ConfigEntityBundleBase implements RemoteDataSetTypeInterface {

  /**
   * A brief description of this remote_data_set type.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

}
