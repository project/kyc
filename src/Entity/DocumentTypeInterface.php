<?php

namespace Drupal\kyc\Entity;

/**
 * Defines the interface for person types.
 */
interface DocumentTypeInterface extends KYCTypeInterface {

}
