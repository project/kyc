<?php

namespace Drupal\kyc\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Defines the interface for Business'ss.
 */
interface KYCInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the state.
   *
   * @return string
   *   The state.
   */
  public function getState();

  /**
   * Sets the state.
   *
   * @param string $state
   *   The state.
   *
   * @return $this
   */
  public function setState($state);

  /**
   * Get first Remote Data Set of a given bundle.
   *
   * @param string $bundle
   *   The bundle.
   *
   * @return entity
   *   The Remote Data Set.
   */
  public function getRemoteDataSet($bundle);

  /**
   * Get all Remote Data Sets.
   *
   * @return array
   *   An array of Remote Data Sets.
   */
  public function getRemoteDataSets();

  /**
   * Get Remote Data Sets of a given bundle.
   *
   * @param string $bundle
   *   The bundle.
   *
   * @return array
   *   An array of Remote Data Sets.
   */
  public function getRemoteDataSetsOfType($bundle);

  /**
   * Set Remote Data Sets for a given bundle.
   *
   * @param string $remote_data_sets
   *   The Remote Data Sets.
   *
   * @return $this
   */
  public function setRemoteDataSets($remote_data_sets);

  /**
   * Adds a remote data set to the list of remote data sets.
   *
   * @param string $remote_data_set
   *   The remote data set.
   *
   * @return $this
   */
  public function addRemoteDataSet($remote_data_set);

  /**
   * Gets the Business's creation timestamp.
   *
   * @return int
   *   The Business's creation timestamp.
   */
  public function getCreatedTime();

  /**
   * Sets the Business's creation timestamp.
   *
   * @param int $timestamp
   *   The Business's creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime($timestamp);

}
