<?php

namespace Drupal\kyc\Entity;

/**
 * Defines the interface for person types.
 */
interface PersonTypeInterface extends KYCTypeInterface {

}
