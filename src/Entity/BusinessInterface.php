<?php

namespace Drupal\kyc\Entity;

use Drupal\address\AddressInterface;

/**
 * Defines the interface for Business'ss.
 */
interface BusinessInterface extends KYCInterface {

  /**
   * Gets the Business's name.
   *
   * @return string
   *   The Business's name.
   */
  public function getName();

  /**
   * Sets the Business's name.
   *
   * @param string $name
   *   The Business's name.
   *
   * @return $this
   */
  public function setName($name);

  /**
   * Gets the Business's email.
   *
   * @return string
   *   The Business's email.
   */
  public function getEmail();

  /**
   * Sets the Business's email.
   *
   * @param string $mail
   *   The Business's email.
   *
   * @return $this
   */
  public function setEmail($mail);

  /**
   * Gets the address.
   *
   * @return \Drupal\address\AddressInterface
   *   The address.
   */
  public function getAddress();

  /**
   * Sets the address.
   *
   * @param \Drupal\address\AddressInterface $address
   *   The address.
   *
   * @return $this
   */
  public function setAddress(AddressInterface $address);

  /**
   * Gets the registration number.
   *
   * @return string
   *   The Business's registration number.
   */
  public function getRegistrationNumber();

  /**
   * Sets the Business's registration number.
   *
   * @param string $number
   *   The Business's registration number.
   *
   * @return $this
   */
  public function setRegistrationNumber($number);

  /**
   * Get persons associated with the Business.
   *
   * @return array
   *   An array of persons.
   */
  public function getPersons();

  /**
   * Get Persons of a given bundle.
   *
   * @param string $bundle
   *   The bundle.
   *
   * @return array
   *   An array of persons.
   */
  public function getPersonsOfType($bundle);

  /**
   * Get first Person of a given bundle.
   *
   * @param string $bundle
   *   The bundle.
   *
   * @return entity
   *   Person.
   */
  public function getPerson($bundle);

  /**
   * Set persons associated with the Business.
   *
   * @param array $persons
   *   An array of persons.
   *
   * @return $this
   */
  public function setPersons($persons);

  /**
   * Get business documents.
   *
   * @return array
   *   An array of documents.
   */
  public function getDocuments();

  /**
   * Set business documents.
   *
   * @param array $documents
   *   An array of documents.
   *
   * @return $this
   */
  public function setDocuments($documents);

}
