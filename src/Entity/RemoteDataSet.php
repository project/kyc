<?php

namespace Drupal\kyc\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\user\EntityOwnerTrait;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the remote_data_set entity class.
 *
 * @ContentEntityType(
 *   id = "kyc_remote_data_set",
 *   label = @Translation("Remote Data Set"),
 *   label_collection = @Translation("Remote Data Sets"),
 *   label_singular = @Translation("remote_data_set"),
 *   label_plural = @Translation("remote_data_sets"),
 *   label_count = @PluralTranslation(
 *     singular = "@count remote_data_set",
 *     plural = "@count remote_data_sets",
 *   ),
 *   bundle_label = @Translation("Remote Data Set type"),
 *   handlers = {
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "query_access" = "Drupal\entity\QueryAccess\QueryAccessHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\kyc\Form\RemoteDataSetForm",
 *       "add" = "Drupal\kyc\Form\RemoteDataSetForm",
 *       "edit" = "Drupal\kyc\Form\RemoteDataSetForm",
 *       "duplicate" = "Drupal\kyc\Form\RemoteDataSetForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\AdminHtmlRouteProvider",
 *       "delete-multiple" = "Drupal\entity\Routing\DeleteMultipleRouteProvider",
 *     }
 *   },
 *   base_table = "kyc_remote_data_set",
 *   admin_permission = "administer remote_data_set",
 *   permission_granularity = "bundle",
 *   translatable = FALSE,
 *   entity_keys = {
 *     "id" = "remote_data_set_id",
 *     "uuid" = "uuid",
 *     "bundle" = "type",
 *     "langcode" = "langcode",
 *     "owner" = "uid",
 *     "uid" = "uid",
 *   },
 *   links = {
 *     "canonical" = "/admin/people/kyc/remote-data-set/{kyc_remote_data_set}",
 *     "add-page" = "/admin/people/kyc/remote-data-set/add",
 *     "add-form" = "/admin/people/kyc/remote-data-set/add/{kyc_remote_data_set_type}",
 *     "edit-form" = "/admin/people/kyc/remote-data-set/{kyc_remote_data_set}/edit",
 *     "duplicate-form" = "/admin/people/kyc/remote-data-set/{kyc_remote_data_set}/duplicate",
 *     "delete-form" = "/admin/people/kyc/remote-data-set/{kyc_remote_data_set}/delete",
 *   },
 *   bundle_entity_type = "kyc_remote_data_set_type",
 *   field_ui_base_route = "entity.kyc_remote_data_set_type.edit_form",
 * )
 */
class RemoteDataSet extends ContentEntityBase implements RemoteDataSetInterface {

  use EntityOwnerTrait;
  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->id();
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteId() {
    return $this->get('remote_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRemoteId($remote_id) {
    $this->set('remote_id', $remote_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteState() {
    return $this->get('remote_state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRemoteState($remote_state) {
    $this->set('remote_state', $remote_state);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['remote_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Remote ID'))
      ->setDescription(t('The remote ID.'))
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['remote_state'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Remote State'))
      ->setDescription(t('The remote state.'))
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when the RemoteDataSet was created.'))
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time when the RemoteDataSet was last edited.'))
      ->setTranslatable(TRUE);

    return $fields;
  }

}
