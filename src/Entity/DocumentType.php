<?php

namespace Drupal\kyc\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the kyc type entity class.
 *
 * @ConfigEntityType(
 *   id = "kyc_document_type",
 *   label = @Translation("Document type"),
 *   label_collection = @Translation("Document types"),
 *   label_singular = @Translation("document type"),
 *   label_plural = @Translation("document types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count document type",
 *     plural = "@count document types",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\entity\BundleEntityAccessControlHandler",
 *     "list_builder" = "Drupal\kyc\DocumentTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\kyc\Form\DocumentTypeForm",
 *       "edit" = "Drupal\kyc\Form\DocumentTypeForm",
 *       "duplicate" = "Drupal\kyc\Form\DocumentTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer kyc_document_type",
 *   config_prefix = "kyc_document_type",
 *   bundle_of = "kyc_document",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "description",
 *     "new_revision",
 *     "workflow",
 *   },
 *   links = {
 *     "add-form" = "/admin/config/people/kyc/document-type/add",
 *     "edit-form" = "/admin/config/people/kyc/document-type/{kyc_document_type}/edit",
 *     "duplicate-form" = "/admin/config/people/kyc/document-type/{kyc_document_type}/duplicate",
 *     "delete-form" = "/admin/config/people/kyc/document-type/{kyc_document_type}/delete",
 *     "collection" = "/admin/config/people/kyc/document-type",
 *   }
 * )
 */
class DocumentType extends ConfigEntityBundleBase implements DocumentTypeInterface {

  /**
   * The document type workflow ID.
   *
   * @var string
   */
  protected $workflow;

  /**
   * A brief description of this document type.
   *
   * @var string
   */
  protected $description;

  /**
   * Default value of the 'Create new revision' checkbox of this node type.
   *
   * @var bool
   */
  protected $new_revision = TRUE;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWorkflowId() {
    return $this->workflow;
  }

  /**
   * {@inheritdoc}
   */
  public function setWorkflowId($workflow_id) {
    $this->workflow = $workflow_id;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setNewRevision($new_revision) {
    $this->new_revision = $new_revision;
  }

  /**
   * {@inheritdoc}
   */
  public function shouldCreateNewRevision() {
    return $this->new_revision;
  }

}
