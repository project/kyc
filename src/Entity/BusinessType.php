<?php

namespace Drupal\kyc\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the business type entity class.
 *
 * @ConfigEntityType(
 *   id = "kyc_business_type",
 *   label = @Translation("Business type"),
 *   label_collection = @Translation("Business types"),
 *   label_singular = @Translation("business type"),
 *   label_plural = @Translation("business types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count business type",
 *     plural = "@count business types",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\entity\BundleEntityAccessControlHandler",
 *     "list_builder" = "Drupal\kyc\BusinessTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\kyc\Form\BusinessTypeForm",
 *       "edit" = "Drupal\kyc\Form\BusinessTypeForm",
 *       "duplicate" = "Drupal\kyc\Form\BusinessTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer kyc_business_type",
 *   config_prefix = "kyc_business_type",
 *   bundle_of = "kyc_business",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "description",
 *     "new_revision",
 *     "workflow",
 *   },
 *   links = {
 *     "add-form" = "/admin/config/people/kyc/business-type/add",
 *     "edit-form" = "/admin/config/people/kyc/business-type/{kyc_business_type}/edit",
 *     "duplicate-form" = "/admin/config/people/kyc/business-type/{kyc_business_type}/duplicate",
 *     "delete-form" = "/admin/config/people/kyc/business-type/{kyc_business_type}/delete",
 *     "collection" = "/admin/config/people/kyc/business-type",
 *   }
 * )
 */
class BusinessType extends ConfigEntityBundleBase implements BusinessTypeInterface {

  /**
   * The business type workflow ID.
   *
   * @var string
   */
  protected $workflow;

  /**
   * A brief description of this business type.
   *
   * @var string
   */
  protected $description;

  /**
   * Default value of the 'Create new revision' checkbox of this node type.
   *
   * @var bool
   */
  protected $new_revision = TRUE;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWorkflowId() {
    return $this->workflow;
  }

  /**
   * {@inheritdoc}
   */
  public function setWorkflowId($workflow_id) {
    $this->workflow = $workflow_id;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setNewRevision($new_revision) {
    $this->new_revision = $new_revision;
  }

  /**
   * {@inheritdoc}
   */
  public function shouldCreateNewRevision() {
    return $this->new_revision;
  }

}
