<?php

namespace Drupal\kyc\Entity;

use CommerceGuys\Addressing\AddressFormat\AddressField;
use CommerceGuys\Addressing\AddressFormat\FieldOverride;
use Drupal\user\EntityOwnerTrait;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\address\AddressInterface;

/**
 * Defines the person entity class.
 *
 * @ContentEntityType(
 *   id = "kyc_person",
 *   label = @Translation("Person"),
 *   label_collection = @Translation("Persons"),
 *   label_singular = @Translation("person"),
 *   label_plural = @Translation("persons"),
 *   label_count = @PluralTranslation(
 *     singular = "@count person",
 *     plural = "@count persons",
 *   ),
 *   bundle_label = @Translation("Person type"),
 *   handlers = {
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "query_access" = "Drupal\entity\QueryAccess\QueryAccessHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\kyc\PersonListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\kyc\Form\PersonForm",
 *       "add" = "Drupal\kyc\Form\PersonForm",
 *       "edit" = "Drupal\kyc\Form\PersonForm",
 *       "duplicate" = "Drupal\kyc\Form\PersonForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\AdminHtmlRouteProvider",
 *       "delete-multiple" = "Drupal\entity\Routing\DeleteMultipleRouteProvider",
 *     }
 *   },
 *   base_table = "kyc_person",
 *   revision_table = "kyc_person_revision",
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer person",
 *   permission_granularity = "bundle",
 *   translatable = FALSE,
 *   entity_keys = {
 *     "id" = "person_id",
 *     "revision" = "revision_id",
 *     "uuid" = "uuid",
 *     "bundle" = "type",
 *     "langcode" = "langcode",
 *     "owner" = "uid",
 *     "uid" = "uid",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "canonical" = "/admin/people/kyc/person/{kyc_person}",
 *     "add-page" = "/admin/people/kyc/person/add",
 *     "add-form" = "/admin/people/kyc/person/add/{kyc_person_type}",
 *     "edit-form" = "/admin/people/kyc/person/{kyc_person}/edit",
 *     "duplicate-form" = "/admin/people/kyc/person/{kyc_person}/duplicate",
 *     "delete-form" = "/admin/people/kyc/person/{kyc_person}/delete",
 *     "collection" = "/admin/people/kyc/person",
 *     "version-history" = "/admin/people/kyc/person/{kyc_person}/revisions",
 *     "revision" = "/admin/people/kyc/person/{kyc_person}/revisions/{kyc_person_revision}/view",
 *   },
 *   bundle_entity_type = "kyc_person_type",
 *   field_ui_base_route = "entity.kyc_person_type.edit_form",
 * )
 */
class Person extends RevisionableContentEntityBase implements PersonInterface {

  use EntityOwnerTrait;
  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function label() {
    return implode(' ', array_filter([
      $this->get('first_name')->value,
      $this->get('middle_name')->value,
      $this->get('last_name')->value,
    ]));
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteId() {
    return $this->get('remote_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRemoteId($remote_id) {
    $this->set('remote_id', $remote_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteState() {
    return $this->get('remote_state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRemoteState($remote_state) {
    $this->set('remote_state', $remote_state);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getState() {
    return $this->get('state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setState($state) {
    $this->set('state', $state);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFirstName() {
    return $this->get('first_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setFirstName($remote_id) {
    $this->set('first_name', $remote_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMiddleName() {
    return $this->get('middle_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMiddleName($remote_id) {
    $this->set('middle_name', $remote_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLastName() {
    return $this->get('last_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLastName($remote_id) {
    $this->set('last_name', $remote_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmail() {
    $email = $this->get('mail')->value;
    // Defaults to the site email if the person email isn't set.
    if (empty($email)) {
      $email = \Drupal::config('system.site')->get('mail') ?: ini_get('sendmail_from');
    }

    return $email;
  }

  /**
   * {@inheritdoc}
   */
  public function setEmail($mail) {
    $this->set('mail', $mail);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDateOfBirth() {
    return $this->get('date_of_birth')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDateOfBirth($date_of_birth) {
    $this->set('date_of_birth', $date_of_birth);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAddress() {
    return $this->get('address')->first();
  }

  /**
   * {@inheritdoc}
   */
  public function setAddress(AddressInterface $address) {
    // $this->set('address', $address) results in the address being appended
    // to the item list, instead of replacing the existing first item.
    $this->address = $address;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getNationalities() {
    $nationalities = [];
    foreach ($this->get('nationalities') as $countryItem) {
      $nationalities[] = $countryItem->value;
    }
    return $nationalities;
  }

  /**
   * {@inheritdoc}
   */
  public function setNationalities($nationalities) {
    $this->set('nationalities', $nationalities);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDocuments() {
    return $this->get('documents')->referencedEntities();
  }

  /**
   * {@inheritdoc}
   */
  public function setDocuments($documents) {
    $this->set('documents', $documents);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteDataSetsOfType($bundle) {
    $remote_data_sets = [];
    foreach ($this->getRemoteDataSets() as $remote_data_set) {
      if ($remote_data_set->bundle() === $bundle) {
        $remote_data_sets[] = $remote_data_set;
      }
    }
    return $remote_data_sets;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteDataSets() {
    return $this->get('remote_data_sets')->referencedEntities();
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteDataSet($bundle) {
    $remote_data_sets = $this->getRemoteDataSetsOfType($bundle);
    return reset($remote_data_sets);
  }

  /**
   * {@inheritdoc}
   */
  public function setRemoteDataSets($remote_data_sets) {
    $this->set('remote_data_sets', $remote_data_sets);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addRemoteDataSet($remote_data_set) {
    $remote_data_sets = $this->getRemoteDataSets();
    $remote_data_sets[] = $remote_data_set;
    $this->setRemoteDataSets($remote_data_sets);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    // Ensure there's a back-reference on each document.
    foreach ($this->documents as $document) {
      $entity = $document->entity;
      if ($entity && $entity->person_id->isEmpty()) {
        $entity->person_id = $this->id();
        $entity->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Type'))
      ->setDescription(t('Person\'s type.'))
      ->setSetting('target_type', 'kyc_person_type')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 0,
      ])
      ->setReadOnly(TRUE);

    $fields['uid']
      ->setLabel(t('Owner'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    // The business backreference, populated by Business::postSave().
    $fields['business_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Business'))
      ->setDescription(t('The parent business.'))
      ->setSetting('target_type', 'kyc_business')
      ->setRevisionable(TRUE)
      ->setReadOnly(TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['state'] = BaseFieldDefinition::create('state')
      ->setLabel(t('State'))
      ->setDescription(t('Verification state.'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -10,
      ])
      ->setDisplayOptions('view', [
        'type' => 'state_transition_form',
        'settings' => [
          'require_confirmation' => TRUE,
          'use_modal' => TRUE,
        ],
        'weight' => -10,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setSetting('workflow_callback', ['\Drupal\kyc\Entity\Person', 'getWorkflowId']);

    $fields['first_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('First name'))
      ->setDescription(t('Person\'s first name.'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['middle_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Middle name'))
      ->setDescription(t('Person\'s middle name.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['last_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Last name'))
      ->setDescription(t('Person\'s last name.'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 2,
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['mail'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Email'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setDescription(t('Person\'s email.'))
      ->setDisplayOptions('form', [
        'type' => 'email_default',
        'weight' => 3,
      ])
      ->setDisplayOptions('view', [
        'type' => 'email_mailto',
        'weight' => 3,
      ])
      ->setSetting('display_description', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['telephone'] = BaseFieldDefinition::create('telephone')
      ->setLabel(t('Telephone number'))
      ->setDescription(t('Business\' telephone number.'))
      ->setRequired(FALSE)
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'telephone_default',
        'weight' => 4,
      ])
      ->setDisplayOptions('view', [
        'type' => 'telephone_link',
        'weight' => 4,
      ])
      ->setSetting('display_description', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['address'] = BaseFieldDefinition::create('address')
      ->setLabel(t('Address'))
      ->setDescription(t('Person\'s address.'))
      ->setCardinality(1)
      ->setRevisionable(TRUE)
      ->setRequired(TRUE)
      ->setSetting('field_overrides', [
        AddressField::GIVEN_NAME => ['override' => FieldOverride::HIDDEN],
        AddressField::ADDITIONAL_NAME => ['override' => FieldOverride::HIDDEN],
        AddressField::FAMILY_NAME => ['override' => FieldOverride::HIDDEN],
        AddressField::ORGANIZATION => ['override' => FieldOverride::HIDDEN],
        AddressField::ADDRESS_LINE3 => ['override' => FieldOverride::HIDDEN],
      ])
      ->setDisplayOptions('form', [
        'type' => 'address_default',
        'weight' => 5,
      ])
      ->setDisplayOptions('view', [
        'type' => 'address_default',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['gender'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Gender'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setSetting('allowed_values',
        [
          'female' => t('Female'),
          'male' => t('Male'),
        ]
      )
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 6,
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['date_of_birth'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Date of birth'))
      ->setDescription(t('Person\'s date of birth'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setSettings([
        'datetime_type' => 'date'
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => 7,
      ])
      ->setDisplayOptions('view', [
        'type' => 'datetime_default',
        'weight' => 7,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['country_of_birth'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Country of birth'))
      ->setSetting('allowed_values_function', ['\Drupal\kyc\Entity\Person', 'getAvailableCountries'])
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 8,
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 8,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['nationalities'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Nationalities'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setSetting('allowed_values_function', ['\Drupal\kyc\Entity\Person', 'getAvailableCountries'])
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 9,
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 9,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['documents'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Documents'))
      ->setRevisionable(TRUE)
      ->setDescription(t('The documents associated with the person.'))
      ->setSetting('target_type', 'kyc_document')
      ->setSetting('handler', 'default')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
        'settings' => [
          'view_mode' => 'default',
        ],
        'weight' => 8,
      ])
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_complex',
        'settings' => [
          'override_labels' => TRUE,
          'label_singular' => 'document',
          'label_plural' => 'documents',
          'add_mode' => 'simple',
          'form_mode' => 'default',
          'allow_new' => TRUE,
          'allow_existing' => TRUE,
          'allow_duplicate' => TRUE,
          'allow_reference' => TRUE,
          'allow_reference_override' => TRUE,
          'available_types' => ['kyc_document'],
          'available_fields' => [
            'name' => 'name',
          ],
        ],
        'weight' => 8,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['remote_data_sets'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Remote Data Sets'))
      ->setDescription(t('The remote data sets associated with person.'))
      ->setSetting('target_type', 'kyc_remote_data_set')
      ->setSetting('handler', 'default')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['path'] = BaseFieldDefinition::create('path')
      ->setLabel(t('URL alias'))
      ->setDescription(t('The person URL alias.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setCustomStorage(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when the Person was created.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time when the Person was last edited.'));

    return $fields;
  }

  /**
   * Gets the allowed values for the 'billing_countries' base field.
   *
   * @return array
   *   The allowed values.
   */
  public static function getAvailableCountries() {
    return \Drupal::service('address.country_repository')->getList();
  }

  /**
   * Gets the workflow ID for the state field.
   *
   * @param \Drupal\kyc\Entity\PersonInterface $person
   *   The person.
   *
   * @return string
   *   The workflow ID.
   */
  public static function getWorkflowId(PersonInterface $person) {
    $person_type = PersonType::load($person->bundle());
    return $person_type?->getWorkflowId() ?? 'kyc_person_default';
  }

}
