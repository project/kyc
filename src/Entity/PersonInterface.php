<?php

namespace Drupal\kyc\Entity;

use Drupal\address\AddressInterface;

/**
 * Defines the interface for Person'ss.
 */
interface PersonInterface extends KYCInterface {

  /**
   * Gets the Person's first name.
   *
   * @return string
   *   The Person's name.
   */
  public function getFirstName();

  /**
   * Sets the Person's first name.
   *
   * @param string $name
   *   The Person's name.
   *
   * @return $this
   */
  public function setFirstName($name);

  /**
   * Gets the Person's middle name.
   *
   * @return string
   *   The Person's name.
   */
  public function getMiddleName();

  /**
   * Sets the Person's middle name.
   *
   * @param string $name
   *   The Person's name.
   *
   * @return $this
   */
  public function setMiddleName($name);

  /**
   * Gets the Person's last name.
   *
   * @return string
   *   The Person's name.
   */
  public function getLastName();

  /**
   * Sets the Person's last  name.
   *
   * @param string $name
   *   The Person's name.
   *
   * @return $this
   */
  public function setLastName($name);

  /**
   * Gets the Person's email.
   *
   * @return string
   *   The Person's email.
   */
  public function getEmail();

  /**
   * Sets the Person's email.
   *
   * @param string $mail
   *   The Person's email.
   *
   * @return $this
   */
  public function setEmail($mail);

  /**
   * Get date of birth.
   *
   * @return string
   *   The date of birth.
   */
  public function getDateOfBirth();

  /**
   * Set date of birth.
   *
   * @param string $date_of_birth
   *   The date of birth.
   *
   * @return $this
   */
  public function setDateOfBirth($date_of_birth);

  /**
   * Gets the address.
   *
   * @return \Drupal\address\AddressInterface
   *   The address.
   */
  public function getAddress();

  /**
   * Sets the address.
   *
   * @param \Drupal\address\AddressInterface $address
   *   The address.
   *
   * @return $this
   */
  public function setAddress(AddressInterface $address);

  /**
   * Get nationalities.
   *
   * @return array
   *   An array of nationalities.
   */
  public function getNationalities();

  /**
   * Set nationalities.
   *
   * @param array $nationalities
   *   An array of nationalities.
   *
   * @return $this
   */
  public function setNationalities($nationalities);

  /**
   * Get documents.
   *
   * @return array
   *   An array of documents.
   */
  public function getDocuments();

  /**
   * Set documents.
   *
   * @param array $documents
   *   An array of documents.
   *
   * @return $this
   */
  public function setDocuments($documents);

}
