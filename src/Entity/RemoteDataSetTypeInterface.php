<?php

namespace Drupal\kyc\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Defines the interface for person types.
 */
interface RemoteDataSetTypeInterface extends ConfigEntityInterface, EntityDescriptionInterface {

}
