<?php

namespace Drupal\kyc\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\kyc\BackendManager;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class EntityTypeSubscriber.
 *
 * @package Drupal\kyc\EventSubscriber
 */
class WorkflowTransitionSubscriber implements EventSubscriberInterface {

  /**
   * Logger factory.
   *
   * @var Psr\Log\LoggerInterface
   */
  private LoggerInterface $logger;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Upsert KYC queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $upsertKYCQueue;

  /**
   * KYC backend manager.
   *
   * @var \Drupal\kyc\BackendManager
   */
  protected $kycBackendManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \Drupal\kyc\BackendManager $kyc_backend_manager
   *   The KYC backend manager.
   */
  public function __construct(
    LoggerChannelFactoryInterface $logger_factory,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    QueueFactory $queue_factory,
    BackendManager $kyc_backend_manager) {
    $this->logger = $logger_factory->get('kyc');
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->upsertKYCQueue = $queue_factory->get('upsert_kyc_queue');
    $this->kycBackendManager = $kyc_backend_manager;
  }

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents() {
    return [
      'kyc_business.submit.pre_transition' => 'submitKYC',
      'kyc_business.reverify.pre_transition' => 'submitKYC',
    ];
  }

  /**
   * Submit KYC callback.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The workflow transition event.
   *
   * @return void
   *   The void.
   */
  public function submitKYC(WorkflowTransitionEvent $event) {
    $entity = $event->getEntity();

    // For each KYC backend, create a queue item to upsert the entity.
    $definitions = $this->kycBackendManager->getDefinitions();
    foreach ($definitions as $definition) {
      $this->upsertKYCQueue->createItem([
        'backend' => $definition['id'],
        'type' => $entity->getEntityTypeId(),
        'id' => $entity->id(),
      ]);
    }
  }

}
