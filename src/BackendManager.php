<?php

namespace Drupal\kyc;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides a KYC Backend plugin manager.
 *
 * @see plugin_api
 */
class BackendManager extends DefaultPluginManager {

  /**
   * Constructs a BackendManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/kyc/Backend',
      $namespaces,
      $module_handler,
      'Drupal\kyc\BackendInterface',
      'Drupal\Component\Annotation\Plugin',
      ['Drupal\kyc\Annotation']
    );
    $this->alterInfo('kyc_backend_info');
    $this->setCacheBackend($cache_backend, 'kyc_backend_info_info_plugins');
  }

}
