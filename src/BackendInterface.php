<?php

namespace Drupal\kyc;

use Drupal\kyc\Entity\KYCInterface;

/**
 * Defines the interface for KYC service.
 */
interface BackendInterface {

  /**
   * Upserts a KYC entity for verification.
   *
   * @param \Drupal\kyc\Entity\KYCInterface $kyc
   *   And entity to upsert.
   */
  public function upsert(KYCInterface $kyc);

}
